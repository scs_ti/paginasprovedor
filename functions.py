import os
import json
import cx_Oracle
import pandas as pd
import sqlalchemy
import sys
from sqlalchemy.pool import NullPool


def get_paramentros_con(database):
    """
    Retorna os paramentros de conexões aos banco de dados
    :return: String
    """
    curr_path = os.path.dirname(os.path.abspath(__file__))
    with open(os.path.join(curr_path, 'pass.json'), 'r') as f:
        parameters = json.load(f)
        user = parameters[database]['user']
        psw = parameters[database]['pass']
        host = parameters[database]['host']
    return user, psw, host


def get_engine_dw():
    """
    Retorna o objeto com a conexão para o banco de dados
    :return: sqlalchemy.engine.base.Engine
    """
    user_dw, psw_dw, host_dw = get_paramentros_con('prd_db_provedor.db')
    try:
        engine_dw = sqlalchemy.create_engine(
            'mysql+pymysql://{}:{}@{}:3306/prd_db_provedor'.format(user_dw, psw_dw, host_dw),
            echo=False, poolclass=NullPool)
    except Exception as e:
        print(e)
        sys.exit()
    return engine_dw

def get_last_id(table, campo, conn):
    sql = ''' SELECT MAX({}) from {} '''.format(campo, table)
    df = pd.read_sql(sql, conn)
    return df


def get_con_database_prdme():
    """
    Retorna o objeto com a conexão para o banco de dados
    :return: sqlalchemy.engine.base.Engine
    """
    curr_path = os.path.dirname(os.path.abspath(__file__))
    with open(os.path.join(curr_path, 'pass.json'), 'r') as f:
        parameters = json.load(f)
    con = cx_Oracle.connect('{}/{}@{}'.format(parameters['prdme.db']['user'],
                                              parameters['prdme.db']['pass'],
                                              parameters['prdme.db']['host']))
    return con


def get_con_database_trne():
    """
    Retorna o objeto com a conexão para o banco de dados
    """
    curr_path = os.path.dirname(os.path.abspath(__file__))
    with open(os.path.join(curr_path, 'pass.json'), 'r') as f:
        parameters = json.load(f)
    con = cx_Oracle.connect('{}/{}@{}'.format(parameters['trnme.db']['user'],
                                              parameters['trnme.db']['pass'],
                                              parameters['trnme.db']['host']))
    return con


def to_staging_area(df, nome_tabela):
    curr_path = os.path.dirname(os.path.abspath(__file__))
    with open(os.path.join(curr_path, 'pass.json'), 'r') as f:
        parameters = json.load(f)
        user = parameters['datamart.db']['user']
        psw = parameters['datamart.db']['pass']
        host = parameters['datamart.db']['host']
    try:
        engine = sqlalchemy.create_engine(
            'mysql+pymysql://{}:{}@{}:3306/staging_area'.format(user, psw, host),
            echo=False, poolclass=NullPool)
    except Exception as e:
        print(e)
        sys.exit()

    df.to_sql('stg_{}'.format(nome_tabela), con=engine, if_exists='replace')


def get_table_fatura(connection):
    sql = '''SELECT F.CD_FATURA,
               F.NR_ANO,
               F.NR_MES,
               F.DT_INICIAL
               FROM DBAPS.FATURA F'''
    df_table = pd.read_sql(sql, connection)
    return df_table


def get_table_lote(connection):
    sql = '''SELECT L.CD_LOTE,
        L.CD_FATURA,
        L.CD_TIPO_ATENDIMENTO
         FROM DBAPS.LOTE L'''
    df_table = pd.read_sql(sql, connection)
    return df_table


def get_table_remessa_prestador(connection):
    sql = '''SELECT REMESSA.CD_REMESSA,
   REMESSA.CD_LOTE,
   REMESSA.TP_SITUACAO,
   REMESSA.CD_CONTRATO,
   REMESSA.CD_PLANO,
   REMESSA.CD_PRESTADOR
    FROM DBAPS.REMESSA_PRESTADOR REMESSA'''
    df_table = pd.read_sql(sql, connection)
    return df_table


def get_table_itremessa_prestador(connection):
    sql = '''
    SELECT 
    ITREMESSA.CD_REMESSA,
    ITREMESSA.TP_SITUACAO,
   ITREMESSA.QT_PAGO,
   ITREMESSA.VL_UNIT_PAGO,
   ITREMESSA.QT_FRANQUIA,
   ITREMESSA.VL_FRANQUIA,
   ITREMESSA.CD_USUARIO,
   ITREMESSA.CD_REMESSA_PAI,
   ITREMESSA.CD_LANCAMENTO_PAI
   FROM DBAPS.ITREMESSA_PRESTADOR ITREMESSA'''
    df_table = pd.read_sql(sql, connection)
    return df_table


def get_table_usuario(connection):
    sql = '''SELECT U.CD_MATRICULA,
       U.DT_NASCIMENTO,
       U.CD_PLANO,
       U.CD_CONTRATO,
       U.NM_CIDADE,
       U.NM_SEGURADO,
       U.TP_SEXO,
       U.CD_EMPRESA,
       U.CD_ESCOLARIDADE
       FROM dbaps.USUARIO U
       '''
    df_table = pd.read_sql(sql, connection)
    return df_table


def get_table_contrato(connection):
    sql = '''SELECT C.CD_CONTRATO,
       C.TP_CONTRATO
       FROM DBAPS.CONTRATO C'''
    df_table = pd.read_sql(sql, connection)
    return df_table


def get_table_prestador(connection):
    sql = '''select
       P.CD_PRESTADOR,
       P.NM_PRESTADOR,
       P.CD_GRUPO_PRESTADOR,
       P.CD_TIP_PRESTADOR
       from DBAPS.PRESTADOR P'''
    df_table = pd.read_sql(sql, connection)
    return df_table


def get_table_tip_prestador(connection):
    sql = '''SELECT TP.CD_TIP_PRESTADOR,
       TP.NM_PRESTADOR
       FROM DBAPS.TIP_PRESTADOR TP'''
    df_table = pd.read_sql(sql, connection)
    return df_table


def get_table_grupo_prestador(connection):
    sql = '''SELECT GP.CD_GRUPO_PRESTADOR,
       GP.DS_GRUPO_PRESTADOR
       FROM DBAPS.GRUPO_PRESTADOR GP'''
    df_table = pd.read_sql(sql, connection)
    return df_table


def get_table_plano(connection):
    sql = '''SELECT P.CD_PLANO,
       P.CD_GRUPO_FRANQUIA,
       P.DS_PLANO
FROM DBAPS.PLANO P'''
    df_table = pd.read_sql(sql, connection)
    return df_table
