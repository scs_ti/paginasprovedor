from functions import *
from querys import *
from sqlalchemy.types import INTEGER
from datetime import datetime

from dateutil.relativedelta import relativedelta

# %% PARAMENTROS DE CONEXÕES AOS BANCO DE DADOS
user_dw,  psw_dw, host_dw = get_paramentros_con('prd_db_provedor.db')
user_oracle,  psw_oracle, host_oracle = get_paramentros_con('prdme.db')

# %%CONEXÃO DO BANCO MYSQL ONDE ESTA O DW
engine_dw = get_engine_dw()

# PARAMETROS
last_month = datetime.now() - relativedelta(months=1)
last2 = int(last_month.strftime('%Y%m'))
curmonth = int(datetime.now().strftime('%Y%m'))

# %%CARGA NA TABELA VALORES_MES
df_meses = pd.read_sql('SELECT t.* FROM dim_tempo t where t.cd_tempo between {} and {}'.format(last2,curmonth), con=engine_dw)
df_valores_mes = pd.DataFrame()
for _,row in df_meses.iterrows():
    print('Pegando valores do mês ',row['cd_tempo'], '.')
    with cx_Oracle.connect('{}/{}@{}'.format(user_oracle, psw_oracle, host_oracle)) as connection:
        cd_tempo = row['cd_tempo']
        df_res = pd.read_sql(sql_fato_valores_mes.format(cd_tempo, cd_tempo, cd_tempo,cd_tempo,cd_tempo), connection)
        df_valores_mes = df_valores_mes.append(df_res)

print('Deletando dos ultimos 2 meses da fato_valores_mes')
with engine_dw.connect() as con:
    result = con.execute('delete FROM fato_valores_mes t where t.cd_tempo >= {}'.format(last2))
print('deletado com sucesso.')


print('executando o carregamento dos dados na tabela fato_valores_mes...')
df_valores_mes.to_sql('fato_valores_mes', con=engine_dw, if_exists='append', index=False, dtype={"CD_TEMPO": INTEGER})
print('carregamento tabela fato_valores_mes finalizado.')


## CORRIGINDO VALORES DIFERENTES DOS MESES ANTERIORES
df_receitas_bi = pd.read_sql('select v.cd_tempo as CD_TEMPO,  v.receita_mes as RECEITA_MES from fato_valores_mes v order by V.CD_TEMPO DESC', con=engine_dw)
with cx_Oracle.connect('{}/{}@{}'.format(user_oracle,psw_oracle,host_oracle)) as con:
    df_receitas_all = pd.read_sql(sql_receitas_all,con)

df_receitas_bi['CD_TEMPO'] = df_receitas_bi['CD_TEMPO'].astype('int64')
for i,row in df_receitas_bi.iterrows():
    print(row['CD_TEMPO'])
    receita = df_receitas_all['RECEITA_MES'].iloc[i]
    tempo = df_receitas_all['CD_TEMPO'].iloc[i]
    print(sql_update_receitas.format(receita, tempo))
    with engine_dw.begin() as con:
        con.execute(sql_update_receitas.format(receita, tempo))

# LINHAS PARA VERIFICAR SE AS ALTERAÇÕES FORAM FEITAS
# for i,row in df_receitas_bi.iterrows():
#     print(df_receitas_all['RECEITA_MES'].iloc[i], ' é = ', row['RECEITA_MES'])
#     print(df_receitas_all['RECEITA_MES'].iloc[i] == row['RECEITA_MES'] )

## CARGA NA TABELA DE VENDAS DE PLANOS
df_meses_02 = pd.read_sql('SELECT t.* FROM dim_tempo t where t.cd_tempo between 201901 and {}'.format(curmonth), con=engine_dw)
df_vendas = pd.DataFrame()
with cx_Oracle.connect('{}/{}@{}'.format(user_oracle, psw_oracle, host_oracle)) as connection:
    for _,row in df_meses_02.iterrows():
       competencia = str(row['cd_tempo'])[4:]+'/'+str(row['cd_tempo'])[0:4]
       df_vendas = df_vendas.append( pd.read_sql(sql_vendas.format(competencia),connection))

print('executando o carregamento dos dados na tabela fato_vendas...')
df_vendas.to_sql('fato_vendas', con=engine_dw, if_exists='replace', index=False, dtype={"CD_TEMPO": INTEGER})
print('carregamento tabela fato_vendas finalizado.')



data_execucao = datetime.today().strftime('%d/%m/%Y %H:%M:00')
data = datetime.strptime(data_execucao,'%d/%m/%Y %H:%M:00')

with engine_dw.connect() as con:
    con.execute('''DELETE FROM historico_execucao H WHERE H.script = 'paginaprincipal' ''')
    con.execute('''INSERT INTO historico_execucao (data_execucao, script) VALUES ('{}', 'paginaprincipal')'''.format(data))
