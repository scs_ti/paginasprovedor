sql_fato_valores_mes = '''
select X.cd_tempo               AS cd_tempo,
       SUM(X.qtd_beneficiarios) AS qtd_beneficiarios,
       SUM(X.receita_mes)       AS receita_mes,
       SUM(X.despesa_mes)       AS despesa_mes,
       SUM(X.vl_copart) AS vl_copart

from (
    -- Beneficiários
     SELECT substr(BI.ANO_MES, 4, 8) || substr(BI.ANO_MES, 1, 2)                                           AS cd_tempo,
            CASE
                WHEN TO_CHAR(BI.PRIMEIRO_DIA_MES, 'MM/YYYY') = TO_CHAR(SYSDATE, 'MM/YYYY')
                    THEN (SELECT COUNT(Q.CD_MATRICULA)
                          FROM DBAPS.USUARIO Q
                          WHERE Q.DT_CADASTRO <= SYSDATE
                            AND Q.CD_MATRICULA NOT IN (-2)
                            AND NOT EXISTS(SELECT 1
                                           FROM DBAPS.DESLIGAMENTO D
                                           WHERE D.CD_MATRICULA = Q.CD_MATRICULA
                                             AND D.DT_REATIVACAO IS NULL
                                             AND D.DT_DESLIGAMENTO <= SYSDATE))
                ELSE (SELECT COUNT(Q.CD_MATRICULA)
                      FROM DBAPS.USUARIO Q
                      WHERE Q.DT_CADASTRO <= LAST_DAY(BI.PRIMEIRO_DIA_MES)
                        AND Q.CD_MATRICULA NOT IN (-2)
                        AND NOT EXISTS(SELECT 1
                                       FROM DBAPS.DESLIGAMENTO D
                                       WHERE D.CD_MATRICULA = Q.CD_MATRICULA
                                         AND D.DT_REATIVACAO IS NULL
                                         AND D.DT_DESLIGAMENTO <= LAST_DAY(LAST_DAY(BI.PRIMEIRO_DIA_MES)))) END AS qtd_beneficiarios,
            0                                                                                                   as receita_mes,
            0                                                                                                   as despesa_mes,
            0 as vl_copart
     FROM DBAPS.CALENDARIO_BI BI
     WHERE BI.ANO || BI.MES = '{}'

         union all
    -- Receitas
     SELECT MC.NR_ANO || MC.NR_MES    as cd_tempo,
            0                         as qtd_beneficiarios,
            SUM(MC.VL_PAGO) AS receita_mes,
            0                         as despesa_mes,
            0 as vl_copart
     FROM DBAPS.CONTRATO C
              INNER JOIN DBAPS.MENS_CONTRATO MC ON MC.CD_CONTRATO = C.CD_CONTRATO
     WHERE MC.NR_ANO || MC.NR_MES = '{}'
     group by MC.NR_ANO || MC.NR_MES

         union all
    -- Despesas
     SELECT A.DT_COMPETENCIA               as cd_tempo,
            0                              as qtd_beneficiarios,
            0                              as receita_mes,
            SUM(ROUND(A.VL_TOTAL_PAGO, 2)) AS despesa_mes,
            0 as vl_copart
     FROM DBAPS.V_CTAS_MEDICAS A
              LEFT JOIN DBAPS.PRESTADOR B ON A.CD_PRESTADOR_PRINCIPAL = B.CD_PRESTADOR
              LEFT JOIN DBAPS.GUIA C ON A.NR_GUIA = C.NR_GUIA
              LEFT JOIN DBAPS.PRESTADOR_ENDERECO D ON C.CD_PRESTADOR_ENDERECO = D.CD_PRESTADOR_ENDERECO
              LEFT JOIN DBAPS.ESPECIALIDADE E ON C.CD_ESPECIALIDADE = E.CD_ESPECIALIDADE
              LEFT JOIN DBAPS.PROCEDIMENTO F ON A.CD_PROCEDIMENTO = F.CD_PROCEDIMENTO
              INNER JOIN DBAPS.REPASSE_PRESTADOR RP ON RP.CD_REPASSE_PRESTADOR = A.CD_REPASSE_PRESTADOR
              LEFT JOIN DBAPS.PRESTADOR P ON P.CD_PRESTADOR = C.CD_PRESTADOR_EXECUTOR
         AND RP.CD_PRESTADOR = A.CD_PRESTADOR_PAGAMENTO
              INNER JOIN DBAPS.PAGAMENTO_PRESTADOR PP ON RP.CD_PAGAMENTO_PRESTADOR = PP.CD_PAGAMENTO_PRESTADOR
              INNER JOIN DBAPS.USUARIO U ON U.CD_MATRICULA = A.CD_MATRICULA
     WHERE A.TP_SITUACAO_CONTA IN ('AA', 'AT')
       AND A.TP_SITUACAO_ITCONTA IN ('AA', 'AT')
       AND A.TP_SITUACAO_EQUIPE IN ('AA', 'AT')
       AND A.TP_ORIGEM = '2'
       AND RP.CD_CON_PAG IS NOT NULL
       AND A.DT_COMPETENCIA = '{}'
       AND A.VL_TOTAL_PAGO > 0
     GROUP BY A.DT_COMPETENCIA

    union all
    -- copart
     SELECT V.DT_COMPETENCIA as cd_tempo,
            0                as qtd_beneficiarios,
            0                AS receita_mes,
            0                as despesa_mes,
            SUM(V.vl_total_franquia) AS "vl_copart"

     FROM DBAPS.V_CTAS_MEDICAS V
     WHERE NVL(V.vl_total_pago, 0) <> 0
       AND V.TP_SITUACAO <> 'NA'
       AND V.vl_total_franquia <> 0
       AND V.DT_COMPETENCIA = '{}'
     group by V.DT_COMPETENCIA
     UNION ALL
     -- FATOR SINISTRO INTERNACAO
     SELECT F.NR_ANO || F.NR_MES as cd_tempo,
            0 as               qtd_beneficiarios,
            0 AS               receita_mes,
            0 as               despesa_mes,
            sum(H.VL_FRANQUIA) AS vl_copart

     FROM DBAPS.CONTA_HOSPITALAR H
              INNER JOIN DBAPS.LOTE L ON L.CD_LOTE = H.CD_LOTE
              INNER JOIN DBAPS.FATURA F ON F.CD_FATURA = L.CD_FATURA
              INNER JOIN DBAPS.USUARIO U ON U.CD_MATRICULA = H.CD_USUARIO
     WHERE F.NR_ANO || F.NR_MES = '{}'

       AND H.CD_MENS_CONTRATO <> 1
       AND NVL(H.VL_FRANQUIA, 0) <> 0
     group by F.NR_ANO || F.NR_MES

    ) X
GROUP BY X.cd_tempo

'''




sql_vendas = '''
SELECT X.cd_tempo,
       X.CLASSIFICACAO,
       COUNT(X.CD_CONTRATO) as QTD_CONTRATOS,
       SUM(X.NRO_VIDAS) AS QUANTIDADE_BENEFICIARIOS
FROM (
SELECT TO_CHAR(C.DT_VENDA,'YYYYMM') as cd_tempo,
       C.CD_CONTRATO,
       C.TP_CONTRATO,
       (SELECT COUNT(U.CD_MATRICULA)
        FROM DBAPS.USUARIO U
        WHERE U.CD_CONTRATO = C.CD_CONTRATO
          AND U.DT_CADASTRO <=
              LAST_DAY(TO_DATE(TO_CHAR('01' || SUBSTR(('{0}'), 0, 2) || SUBSTR(('{0}'), 4, 7)), 'DD/MM/YYYY'))
          AND U.SN_ATIVO = 'S') AS "NRO_VIDAS",

       CASE
           WHEN C.TP_CONTRATO = 'I' THEN 'PF'
           WHEN C.TP_CONTRATO = 'A' THEN 'ADESAO'
           WHEN C.TP_CONTRATO = 'E' AND (SELECT COUNT(U.CD_MATRICULA)
                                         FROM DBAPS.USUARIO U
                                         WHERE U.CD_CONTRATO = C.CD_CONTRATO
                                           AND U.DT_CADASTRO >= TO_DATE(TO_CHAR(
                                                                                '01' || SUBSTR(('{0}'), 0, 2) || SUBSTR(('{0}'), 4, 7)),
                                                                        'DD/MM/YYYY')
                                           AND U.DT_CADASTRO <= LAST_DAY(TO_DATE(TO_CHAR(
                                                                                         '01' || SUBSTR(('{0}'), 0, 2) || SUBSTR(('{0}'), 4, 7)),
                                                                                 'DD/MM/YYYY'))) >= 30 THEN 'PJ'
           ELSE 'PME'
           END                     "CLASSIFICACAO"
FROM DBAPS.CONTRATO C
WHERE C.DT_VENDA >= TO_DATE(TO_CHAR('01' || SUBSTR(('{0}'), 0, 2) || SUBSTR(('{0}'), 4, 7)), 'DD/MM/YYYY')
  AND C.DT_VENDA <=
      LAST_DAY(TO_DATE(TO_CHAR('01' || SUBSTR(('{0}'), 0, 2) || SUBSTR(('{0}'), 4, 7)), 'DD/MM/YYYY'))
  AND C.SN_DEMITIDO_APOSENTADO_OBITO <> 'S'
  AND (SELECT COUNT(U.CD_MATRICULA)
       FROM DBAPS.USUARIO U
       WHERE U.CD_CONTRATO = C.CD_CONTRATO
         AND U.DT_CADASTRO >= TO_DATE(TO_CHAR('01' || SUBSTR(('{0}'), 0, 2) || SUBSTR(('{0}'), 4, 7)), 'DD/MM/YYYY')
         AND U.DT_CADASTRO <= LAST_DAY(TO_DATE(TO_CHAR('01' || SUBSTR(('{0}'), 0, 2) || SUBSTR(('{0}'), 4, 7)), 'DD/MM/YYYY'))) <> 0) X
GROUP BY X.cd_tempo, X.CLASSIFICACAO

'''



sql_receitas_all = '''
SELECT MC.NR_ANO || MC.NR_MES    as cd_tempo,
       0 as temp,
            SUM(MC.VL_PAGO) AS receita_mes

     FROM DBAPS.CONTRATO C
              INNER JOIN DBAPS.MENS_CONTRATO MC ON MC.CD_CONTRATO = C.CD_CONTRATO
     WHERE MC.NR_ANO || MC.NR_MES between '201901' and to_char(sysdate, 'yyyymm')
     group by MC.NR_ANO || MC.NR_MES
     ORDER BY MC.NR_ANO || MC.NR_MES DESC
'''


sql_update_receitas = '''
update fato_valores_mes r 
set r.receita_mes = {0}
where r.cd_tempo = {1}
'''